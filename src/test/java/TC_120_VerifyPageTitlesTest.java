import models.RegisteredUser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pages.*;

public class TC_120_VerifyPageTitlesTest extends BaseTest{

    RegisteredUser user = new RegisteredUser();
    LoginPage loginPage;
    SearchHotelPage searchPage;
    SelectHotelPage selectPage;
    BookHotelPage bookPage;
    BookingConfirmationPage confirmationPage;
    BookedItineraryPage itineraryPage;

    @Before
    public void initializeTest() {
        super.initializeTest();
        loginPage = new LoginPage(driver, wait);
        searchPage = new SearchHotelPage(driver, wait);
        selectPage = new SelectHotelPage(driver, wait);
        bookPage = new BookHotelPage(driver, wait);
        confirmationPage = new BookingConfirmationPage(driver, wait);
        itineraryPage = new BookedItineraryPage(driver, wait);
    }

    @Test
    public void verifyPageTitles(){
        loginPage.login(user.userName, user.userPass);
        BasePage page = new BasePage(driver, wait);
        page.verifyPageTitleIs("AdactIn.com - Search Hotel");

        searchPage.searchHotel("Sydney", "Hotel Creek", "Standard", "1 - One", BasePage.getCurrentDate(), BasePage.getCurrentDatePlusXDays(1));
        page.verifyPageTitleIs("AdactIn.com - Select Hotel");

        selectPage.selectHotelNumber(1);
        page.verifyPageTitleIs("AdactIn.com - Book A Hotel");

        String orderNumber = bookPage.bookHotel("Nataliya", "Sunny", "MyAddress", "1111222233334444", "VISA", "June", "2018", "111");
        page.verifyPageTitleIs("AdactIn.com - Hotel Booking Confirmation");

        confirmationPage.clickMyItineraryBtn();
        page.verifyPageTitleIs("AdactIn.com - Select Hotel");

        itineraryPage.searchBooking(orderNumber);
        confirmationPage.logoutViaLink();
        page.verifyPageTitleIs("AdactIn.com - Logout");
    }

    @After
    public void afterTest() {
        driver.quit();
    }

}
