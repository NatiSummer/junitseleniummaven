import models.RegisteredUser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pages.*;

public class TC_109_VerifyLogoutTest extends BaseTest {

    RegisteredUser user = new RegisteredUser();
    LoginPage loginPage;
    SearchHotelPage searchPage;
    SelectHotelPage selectPage;
    BookHotelPage bookPage;
    BookingConfirmationPage confirmationPage;

    @Before
    public void initializeTest() {
        super.initializeTest();
        loginPage = new LoginPage(driver, wait);
        searchPage = new SearchHotelPage(driver, wait);
        selectPage = new SelectHotelPage(driver, wait);
        bookPage = new BookHotelPage(driver, wait);
        confirmationPage = new BookingConfirmationPage(driver, wait);
    }

    @Test
    public void verifySuccessfulLogout() {
        loginPage.login(user.userName, user.userPass);
        searchPage.searchHotel("Sydney", "Hotel Creek", "Standard", "1 - One", BasePage.getCurrentDate(), BasePage.getCurrentDatePlusXDays(1));
        selectPage.selectHotelNumber(1);
        bookPage.bookHotel("Nataliya", "Sunny", "MyAddress", "1111222233334444", "VISA", "June", "2018", "111");
        confirmationPage.logoutViaLink();
    }

    @After
    public void afterTest() {
        driver.quit();
    }

}
