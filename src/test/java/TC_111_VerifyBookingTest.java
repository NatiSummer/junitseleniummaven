import models.RegisteredUser;
import org.junit.Before;
import org.junit.Test;
import pages.*;

public class TC_111_VerifyBookingTest extends BaseTest{

    RegisteredUser user = new RegisteredUser();

    LoginPage loginPage;
    SearchHotelPage searchPage;
    SelectHotelPage selectPage;
    BookHotelPage bookPage;

    @Before
    public void initializeTest() {
        super.initializeTest();
        loginPage = new LoginPage(driver, wait);
        searchPage = new SearchHotelPage(driver, wait);
        selectPage = new SelectHotelPage(driver, wait);
        bookPage = new BookHotelPage(driver,wait);
    }

    @Test
    public void verifyBookingData() {
        loginPage.login(user.userName, user.userPass);
        searchPage.searchHotel("Sydney", "Hotel Creek", "Standard", "1 - One", BasePage.getCurrentDate(), BasePage.getCurrentDatePlusXDays(1));
        selectPage.selectHotelNumber(1);
        bookPage.verifyHotelDetailsOnBookingPage("Hotel Creek", "Sydney", "Standard", "1 Room(s)", "1 Day(s)", "AUD $ 125", "AUD $ 135");
    }

    @Test
    public void verifyOrderNumberGenerated() {
        loginPage.login(user.userName, user.userPass);
        searchPage.searchHotel("Sydney", "Hotel Creek", "Standard", "1 - One", "15/03/2018", "17/03/2018");
        selectPage.selectHotelNumber(1);
        bookPage.bookHotel("Nataliya", "Sunny", "MyAddress", "1111222233334444", "VISA", "June", "2018", "111");
    }

}
