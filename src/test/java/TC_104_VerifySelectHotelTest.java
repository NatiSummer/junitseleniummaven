import models.RegisteredUser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pages.*;

public class TC_104_VerifySelectHotelTest extends BaseTest {

    RegisteredUser user = new RegisteredUser();
    LoginPage loginPage;
    SearchHotelPage searchPage;
    SelectHotelPage selectPage;

    @Before
    public void initializeTest() {
        super.initializeTest();
        loginPage = new LoginPage(driver, wait);
        searchPage = new SearchHotelPage(driver, wait);
        selectPage = new SelectHotelPage(driver, wait);
    }

    @Test
    public void verifySelectedHotelDetails() {
        loginPage.login(user.userName, user.userPass);
        searchPage.searchHotel("Sydney", "Hotel Creek", "Standard", "1 - One", BasePage.getCurrentDate(), BasePage.getCurrentDatePlusXDays(1));
        selectPage.verifyHotelDetailsOnSelectHotelPage("Hotel Creek", "Sydney", "1 Rooms", BasePage.getCurrentDate(), BasePage.getCurrentDatePlusXDays(1), "1 Days", "Standard");
        BasePage.takeScreenshot(driver, "TC_104_SelectHotelDetails");
    }

    @Test
    public void verifyTotalPriceCalculation() throws InterruptedException {
        loginPage.login(user.userName, user.userPass);
        searchPage.searchHotel("Sydney", "Hotel Creek", "Standard", "1 - One", BasePage.getCurrentDate(), BasePage.getCurrentDatePlusXDays(2));
        selectPage.verifyTotalPrice(260);
    }

    @After
    public void afterTest() {
        driver.quit();
    }

}
