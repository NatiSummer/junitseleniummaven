import models.RegisteredUser;
import org.junit.Before;
import org.junit.Test;
import pages.LoginPage;

public class TC_101_VerifyLoginTest extends BaseTest{

    RegisteredUser user = new RegisteredUser();
    LoginPage loginPage;

    @Before
    public void initializeTest() {
        super.initializeTest();
        loginPage = new LoginPage(driver, wait);
    }

    @Test
    public void verifySuccessfulLogin() {
        loginPage.login(user.userName, user.userPass);
    }

}
