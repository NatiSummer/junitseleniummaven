package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import static junit.framework.Assert.assertEquals;

public class SelectHotelPage extends BasePage{

    public SelectHotelPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(id="continue")
    public WebElement continueBtn;

    public void verifyHotelDetailsOnSelectHotelPage(String hotel, String location, String rooms, String arrDate, String depDate, String noOfDays, String roomType) {
        assertEquals("Hotel mismatch: ", hotel, driver.findElement(By.id("hotel_name_0")).getAttribute("value"));
        assertEquals("Location mismatch: ", location, driver.findElement(By.id("location_0")).getAttribute("value"));
        assertEquals("Rooms mismatch: ", rooms, driver.findElement(By.id("rooms_0")).getAttribute("value"));
        assertEquals("Arrival date mismatch: ", arrDate, driver.findElement(By.id("arr_date_0")).getAttribute("value"));
        assertEquals("Departure date mismatch: ", depDate, driver.findElement(By.id("dep_date_0")).getAttribute("value"));
        assertEquals("Num of days mismatch: ", noOfDays, driver.findElement(By.id("no_days_0")).getAttribute("value"));
        assertEquals("Rooms type mismatch: ", roomType, driver.findElement(By.id("room_type_0")).getAttribute("value"));
    }

    public void verifyTotalPrice(int expTotalPrice){
        String total = driver.findElement(By.id("total_price_0")).getAttribute("value");
        String actTotal = total.substring(6);
        Assert.assertEquals("Total amount is incorrect: ", expTotalPrice + "$", actTotal + "$");
    }

    public void selectHotelNumber(int hotelNumber) {
        driver.findElement(By.id("radiobutton_" + (hotelNumber - 1))).click();
        continueBtn.click();
    }
}
