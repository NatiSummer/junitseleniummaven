package pages;

import org.apache.maven.shared.utils.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static org.openqa.selenium.support.PageFactory.initElements;

public class BasePage {

    public WebDriver driver;
    public WebDriverWait wait;

    public BasePage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        initElements(driver, this);
    }

    public void verifyPageTitleIs(String expPageTitle){
        Assert.assertEquals("Title mismatch: ", expPageTitle, driver.getTitle());
    }

    public static void takeScreenshot(WebDriver driver, String screenshotName) {
        String msg, path;
        try {
            File source = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            path = "./ScreenShots/" + screenshotName + ".png";
            FileUtils.copyFile(source, new File(path));
            msg = "Screenshot captured at" + path;
        } catch (IOException e) {
            msg = "Failed to capture screenshot: " + e.getMessage();
        }
        System.out.println(msg);
    }

    public static String getCurrentDate(){
        Calendar date = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String currentDate = dateFormat.format(date.getTime());
        System.out.println("Current Date: " + currentDate);
        return currentDate;
    }

    public static String getCurrentDatePlusXDays(int plusXDays){
        Calendar date = Calendar.getInstance();
        date.add(Calendar.DATE, plusXDays);
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String datePlusXDays = dateFormat.format(date.getTime());
        System.out.println("Current Date + " + plusXDays + " day(s): " + datePlusXDays);
        return datePlusXDays;
    }

}
