package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static junit.framework.Assert.assertEquals;

public class SearchHotelPage extends BasePage{

    String checkInMsgId = "checkin_span";

    public SearchHotelPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(id="location")
    public WebElement hotelLocation;

    @FindBy(id="hotels")
    public WebElement hotelName;

    @FindBy(id="room_type")
    public WebElement roomTypeId;

    @FindBy(id="room_nos")
    public WebElement numberOfRooms;

    @FindBy(id="datepick_in")
    public WebElement checkInDate;

    @FindBy(id="datepick_out")
    public WebElement checkOutDate;

    @FindBy(id="Submit")
    public WebElement submitBtn;

    public void searchHotel(String location, String hotel, String roomType, String numOfRoom, String checkIn, String checkOut) {
        new Select(hotelLocation).selectByVisibleText(location);
        new Select(hotelName).selectByVisibleText(hotel);
        new Select(roomTypeId).selectByVisibleText(roomType);
        new Select(numberOfRooms).selectByVisibleText(numOfRoom);
        checkInDate.clear();
        checkInDate.sendKeys(checkIn);
        checkOutDate.clear();
        checkOutDate.sendKeys(checkOut);
        submitBtn.submit();
    }

    public void verifyErrorMessage(String errMessage){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(checkInMsgId)));
        String actErrorMsg = driver.findElement(By.id(checkInMsgId)).getText();
        assertEquals("Error message is incorrect: ", errMessage, actErrorMsg);
    }
}
