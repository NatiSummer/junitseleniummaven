package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BookingConfirmationPage extends BasePage {


    public BookingConfirmationPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(linkText = "Logout")
    public WebElement logoutLnk;

    @FindBy(xpath = "//*[@class=\"reg_success\"]")
    public WebElement successMessageLogout;

    @FindBy(id = "my_itinerary")
    public WebElement myItineraryBtn;

    public void logoutViaLink(){
        logoutLnk.click();
        wait.until(ExpectedConditions.textToBePresentInElement(successMessageLogout,"You have successfully logged out"));
    }

    public void clickMyItineraryBtn(){
        myItineraryBtn.click();
    }
}
