package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

public class BookHotelPage extends BasePage {

    public BookHotelPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(id = "hotel_name_dis")
    public WebElement hotelName;

    @FindBy(id = "location_dis")
    public WebElement hotelLocation;

    @FindBy(id = "room_type_dis")
    public WebElement typeOfRoom;

    @FindBy(id = "room_num_dis")
    public WebElement roomNum;

    @FindBy(id = "total_days_dis")
    public WebElement totalDays;

    @FindBy(id = "price_night_dis")
    public WebElement pricePerNightDol;

    @FindBy(id = "total_price_dis")
    public WebElement totalPriceDol;


    @FindBy(id = "first_name")
    public WebElement firstNameId;

    @FindBy(id = "last_name")
    public WebElement lastNameId;

    @FindBy(id = "address")
    public WebElement addressId;

    @FindBy(id = "cc_num")
    public WebElement ccNumberId;

    @FindBy(id = "cc_type")
    public WebElement ccTypeId;

    @FindBy(id = "cc_exp_month")
    public WebElement ccExpMonthId;

    @FindBy(id = "cc_exp_year")
    public WebElement ccExpYearId;

    @FindBy(id = "cc_cvv")
    public WebElement ccCvvId;

    @FindBy(id = "book_now")
    public WebElement bookNowBtn;

    @FindBy(id = "order_no")
    public WebElement ordernNumberId;

    public String bookHotel(String firstName, String lastName, String address, String ccNum, String ccType, String ccExtMonth, String ccExpYear, String ccCVV) {
        firstNameId.sendKeys(firstName);
        lastNameId.sendKeys(lastName);
        addressId.sendKeys(address);
        ccNumberId.sendKeys(ccNum);
        new Select(ccTypeId).selectByVisibleText(ccType);
        new Select(ccExpMonthId).selectByVisibleText(ccExtMonth);
        new Select(ccExpYearId).selectByVisibleText(ccExpYear);
        ccCvvId.sendKeys(ccCVV);

        bookNowBtn.submit();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("order_no")));
        String orderNumber = ordernNumberId.getAttribute("value");
        assertTrue("Order number isn't generated!", !orderNumber.equals(""));
        return orderNumber;
    }

    public void verifyHotelDetailsOnBookingPage(String hotel, String location, String roomType, String roomsNum, String days, String pricePerNight, String totalPrice) {
        assertEquals("Hotel mismatch: ", hotel, hotelName.getAttribute("value"));
        assertEquals("Location mismatch: ", location, hotelLocation.getAttribute("value"));
        assertEquals("Room type mismatch: ", roomType, typeOfRoom.getAttribute("value"));
        assertEquals("Rooms number mismatch: ", roomsNum, roomNum.getAttribute("value"));
        assertEquals("Days mismatch: ", days, totalDays.getAttribute("value"));
        assertEquals("Price per night mismatch: ", pricePerNight, pricePerNightDol.getAttribute("value"));
        assertEquals("Total price mismatch: ", totalPrice, totalPriceDol.getAttribute("value"));
    }
}
