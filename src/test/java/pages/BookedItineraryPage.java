package pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

public class BookedItineraryPage extends BasePage{

    public BookedItineraryPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(id="order_id_text")
    public WebElement searchField;

    @FindBy(id="search_hotel_id")
    public WebElement searchBtn;

    @FindBy(xpath = "//*[@id=\"booked_form\"]/table/tbody/tr[2]/td/table/tbody/tr")
    public List<WebElement> tableRows;

    @FindBy(xpath="//input[contains(@id,'btn_id')]/../..//input[contains(@id,'order_id')]")
    public WebElement orderId;

    @FindBy(xpath="//input[contains(@id,'hotel_name')]")
    public WebElement hotelName;

    @FindBy(xpath="//input[contains(@id,'location')]")
    public WebElement hotelLocation;

    @FindBy(xpath="//input[contains(@id,'rooms')]")
    public WebElement roomsNum;

    @FindBy(xpath="//input[contains(@id,'first_name')]")
    public WebElement firstNameId;

    @FindBy(xpath="//input[contains(@id,'last_name')]")
    public WebElement lastNameId;

    @FindBy(xpath="//input[contains(@id,'arr_date')]")
    public WebElement arrivalDate;

    @FindBy(xpath="//input[contains(@id,'dep_date')]")
    public WebElement departureDate;

    @FindBy(xpath="//input[contains(@id,'no_days')]")
    public WebElement numberOfDays;

    @FindBy(xpath="//input[contains(@id,'room_type')]")
    public WebElement roomTypeId;

    @FindBy(xpath="//input[contains(@id,'price_night')]")
    public WebElement pricePerNightId;

    @FindBy(xpath="//input[contains(@id,'total_price')]")
    public WebElement totalPriceId;


    public void searchBooking(String orderNumber){
        searchField.sendKeys(orderNumber);
        searchBtn.click();
        int tableSize = tableRows.size();
        assertTrue((tableSize - 1) == 1);
    }

    public void verifyBookingDetails(String orderNumber, String hotel, String location, String rooms, String firstName, String lastName, String arrDate,
                                     String depDate, String numOfDays, String roomType, String pricePerNight, String totalPrice){
        assertEquals("Order Id mismatch: ", orderNumber, orderId.getAttribute("value"));
        assertEquals("Hotel Name mismatch: ", hotel, hotelName.getAttribute("value"));
        assertEquals("Location mismatch: ", location, hotelLocation.getAttribute("value"));
        assertEquals("Rooms mismatch: ", rooms, roomsNum.getAttribute("value"));
        assertEquals("First Name mismatch: ", firstName, firstNameId.getAttribute("value"));
        assertEquals("Last Name mismatch: ", lastName, lastNameId.getAttribute("value"));
        assertEquals("Arrival Date mismatch: ", arrDate, arrivalDate.getAttribute("value"));
        assertEquals("Departure Date mismatch: ", depDate, departureDate.getAttribute("value"));
        assertEquals("Number of Days mismatch: ", numOfDays, numberOfDays.getAttribute("value"));
        assertEquals("Room Type mismatch: ", roomType, roomTypeId.getAttribute("value"));
        assertEquals("Price Per Night mismatch: ", pricePerNight, pricePerNightId.getAttribute("value"));
        assertEquals("Total Price mismatch: ", totalPrice, totalPriceId.getAttribute("value"));
    }

    public void cancelOrderNumber(String orderNumber){
        driver.findElement(By.xpath("//input[@value=\"Cancel " + orderNumber + "\"]")).click();
        Alert javascriptAlert = driver.switchTo().alert();
        System.out.println("javascript alert: " + javascriptAlert.getText());
        javascriptAlert.accept();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("search_result_error")));
        assertEquals("Error message is incorrect: ", "The booking has been cancelled.", driver.findElement(By.id("search_result_error")).getText());

        //TODO - verification of not displayed element doesn't work, why?
//        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@value=\"" + orderNumber + "\"]")));
    }

}
