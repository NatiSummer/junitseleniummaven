package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static junit.framework.Assert.assertEquals;

public class LoginPage extends BasePage {

    String appURL = "http://www.adactin.com/HotelApp/";

    public LoginPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(id = "username")
    public WebElement login;

    @FindBy(id="password")
    public WebElement password;

    @FindBy(id="login")
    public WebElement loginBtn;

    @FindBy(id="username_show")
    public WebElement welcomeMsg;

    public void login(String userName, String userPass) {
        driver.get(appURL);
        login.sendKeys(userName);
        password.sendKeys(userPass);
        loginBtn.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username_show")));
        String actWelcomeText = welcomeMsg.getAttribute("value");
        assertEquals("Welcome text is incorrect: ", "Hello Nataliya!", actWelcomeText);
    }

}
