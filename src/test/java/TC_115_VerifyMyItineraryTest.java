import models.RegisteredUser;
import org.junit.*;
import org.openqa.selenium.By;
import pages.*;

public class TC_115_VerifyMyItineraryTest extends BaseTest {

    RegisteredUser user = new RegisteredUser();
    LoginPage loginPage;
    SearchHotelPage searchPage;
    SelectHotelPage selectPage;
    BookHotelPage bookPage;
    BookingConfirmationPage confirmationPage;
    BookedItineraryPage itineraryPage;

    @Before
    public void initializeTest() {
        super.initializeTest();
        loginPage = new LoginPage(driver, wait);
        searchPage = new SearchHotelPage(driver, wait);
        selectPage = new SelectHotelPage(driver, wait);
        bookPage = new BookHotelPage(driver, wait);
        confirmationPage = new BookingConfirmationPage(driver, wait);
        itineraryPage = new BookedItineraryPage(driver, wait);
    }

    @Test
    @Ignore
    public void verifyInfoOnMyItineraryNotEditable() {
        loginPage.login(user.userName, user.userPass);
        searchPage.searchHotel("Sydney", "Hotel Creek", "Standard", "1 - One", BasePage.getCurrentDate(), BasePage.getCurrentDatePlusXDays(1));
        selectPage.selectHotelNumber(1);
        String orderNumber = bookPage.bookHotel("Nataliya", "Sunny", "MyAddress", "1111222233334444", "VISA", "June", "2018", "111");
        confirmationPage.clickMyItineraryBtn();
        itineraryPage.searchBooking(orderNumber);
        // Bug in the app, fields are editable
        Boolean isDisabled = !(driver.findElement(By.xpath("//input[contains(@id,'btn_id')]/../..//input[contains(@id,'order_id')]")).isEnabled());
        Assert.assertTrue("Element NOT disabled.", isDisabled);
    }

    @Test
    public void verifyBookedItineraryDetails() {
        loginPage.login(user.userName, user.userPass);
        searchPage.searchHotel("Sydney", "Hotel Creek", "Standard", "1 - One", BasePage.getCurrentDate(), BasePage.getCurrentDatePlusXDays(1));
        selectPage.selectHotelNumber(1);
        String orderNumber = bookPage.bookHotel("Nataliya", "Sunny", "MyAddress", "1111222233334444", "VISA", "June", "2018", "111");
        confirmationPage.clickMyItineraryBtn();
        itineraryPage.searchBooking(orderNumber);
        itineraryPage.verifyBookingDetails(orderNumber, "Hotel Creek", "Sydney", "1 Rooms", "Nataliya", "Sunny", BasePage.getCurrentDate(), BasePage.getCurrentDatePlusXDays(1),
                "1 Days", "Standard", "AUD $ 125", "AUD $ 149");
    }

    @Test
    public void verifyOrderCancellation() {
        loginPage.login(user.userName, user.userPass);
        searchPage.searchHotel("Sydney", "Hotel Creek", "Standard", "1 - One", BasePage.getCurrentDate(), BasePage.getCurrentDatePlusXDays(1));
        selectPage.selectHotelNumber(1);
        String orderNumber = bookPage.bookHotel("Nataliya", "Sunny", "MyAddress", "1111222233334444", "VISA", "June", "2018", "111");
        confirmationPage.clickMyItineraryBtn();
        itineraryPage.searchBooking(orderNumber);
        itineraryPage.cancelOrderNumber(orderNumber);
    }

    @After
    public void afterTest() {
        driver.quit();
    }

}
