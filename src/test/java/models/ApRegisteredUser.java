package models;

//TODO - clarify or delete
public enum ApRegisteredUser {

    User("Nataliya", "Adactin");

    private final String userName;
    private final String userPass;

    private ApRegisteredUser(String userName, String userPass) {
        this.userName = userName;
        this.userPass = userPass;
    }

    public String getUserName(){
        return userName;
    }

    public String getUserPass(){
        return userPass;
    }

}
