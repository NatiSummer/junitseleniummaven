import models.RegisteredUser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pages.BasePage;
import pages.LoginPage;
import pages.SearchHotelPage;

public class TC_102_VerifySearchHotelTest extends BaseTest {

    RegisteredUser user = new RegisteredUser();
    LoginPage loginPage;
    SearchHotelPage searchPage;

    @Before
    public void initializeTest() {
        super.initializeTest();
        loginPage = new LoginPage(driver, wait);
        searchPage = new SearchHotelPage(driver, wait);
    }

    @Test
    public void verifyCheckInDateLaterThanCheckOut() throws InterruptedException {
        loginPage.login(user.userName, user.userPass);
        searchPage.searchHotel("Sydney", "Hotel Creek", "Standard", "1 - One", BasePage.getCurrentDatePlusXDays(7), BasePage.getCurrentDatePlusXDays(5));
        searchPage.verifyErrorMessage("Check-In Date shall be before than Check-Out Date");
        BasePage.takeScreenshot(driver, "TC_102_ErrorMessage");
    }

    @After
    public void afterTest() {
        driver.quit();
    }

}
