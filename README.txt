====================================================================

Automation Framework sample, using:
+ maven
+ jUnit 
+ Selenium WebDriver 3.9.0
+ PageObject with PageFactory

====================================================================

TODO:
1. add BDD support: Cucumber or jBehave
2. set up Jenkins job for running test suite
3. configure Seleniu Grid

====================================================================

1. create new Maven project and create 1st test class under src/test/java -> e.g. TC_101_VerifyLoginTest.java

2. add maven dependencies in pom or via java class:
- 'selenium-api' to use WebDriver class
- 'selenium-chrome-driver' to use ChromeDriver class
- 'junit' to use its annotations like @Test
- 'selenium-support' to use Select for UI elements

3. TC_101_VerifyLoginTest.java
- create new WebDriver Chrome instance, WebDriver driver = new ChromeDriver();
- create first @Test (junit)
- create @After method to quit/close WebDriver (junit)
- create @Before method to initialize WebDriver, WebDriverWait and implicit wait

4. create .gitignore.txt file:
*.idea
*.DS_Store
*.iml
target/

5. create 2nd test class, e.g. TC_102

// Create separate *.java class with methods OR go to step 8
6. move code from test classes to new class with test methods, e.g. BusinessMethods.java (it should declare WebDriver)
7. Test classes shoule extend BusinessMethods

// Create PageObject with PageFactory
8. Create BaseTest.java
8.1 declare there
	WebDriver driver;
    WebDriverWait wait;
8.2 specify there @Before and @After methods for each test class
    @Before
    public void initializeTest(){
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);}

    @After
    public void afterTest() {driver.quit();}
8.3 inherit from BaseTest.java each Test class
8.4 in each Test class specify @Before method 
	- call super @Before method
	- initialize in test class @Before method object for each page, e.g.
   		LoginPage loginPage;

    	@Before
    	public void initializeTest() {
        	super.initializeTest();
        	loginPage = new LoginPage(driver, wait);}

9. Create BasePage.java
9.1 declare there 
	WebDriver driver;
	WebDriverWait wait;
9.2 add super constructor and pass there driver and wait
	public BasePage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        initElements(driver, this);}
9.3 add there all common methods, e.g. getCurrentDate()
9.4 inherit from BasePage.java each Page class
9.5 in each Page class add constructor and pass there driver and wait
	public LoginPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);}

